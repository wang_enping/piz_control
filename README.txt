Step 1: 安装YOLOX

(have downloaded)git clone https://github.com/Megvii-BaseDetection/YOLOX.git
cd YOLOX
pip3 install -U pip && pip3 install -r requirements.txt
pip3 install -v -e .  # or  python3 setup.py develop
pip3 install cython; pip3 install 'git+https://github.com/cocodataset/cocoapi.git#subdirectory=PythonAPI'



Step 2: 

source /opt/ros/noetic/setup.bash
mkdir -p ~/ros1_ws/src/ && cd ~/ros1_ws/src/
(have downloaded)git clone https://github.com/Ar-Ray-code/YOLOX-ROS.git -b noetic --recursive
cd ~/ros1_ws
put yolox_voc_s.py to YOLOX-ROS/exps
put best_ckpt.pth to YOLOX-ROS/weights
catkin_make
(sudo apt update && sudo apt install ros-noetic-cv-bridge  )


Step 3: test

source devel/setup.bash
roslaunch yolox_ros_py demo_yolox_s.launch
roslaunch image_pub image_pub.launch



