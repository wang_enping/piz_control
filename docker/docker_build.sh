#!/bin/bash
#
# This script runs docker build to create the comac docker image.
#

set -exu   # http://linuxcommand.org/lc3_man_pages/seth.html

tag_name=jchen0213/tele-opt2:ur_moveit_1129

cd ..

docker build -f docker_ur5e/Dockerfile --tag ${tag_name} .

#docker image push jchen0213/tele-opt2:dev
