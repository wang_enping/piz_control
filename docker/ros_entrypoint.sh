#!/bin/bash
set -e

# setup ros environment
#source /opt/ros/galactic/setup.bash
source /root/im_ws/install/setup.bash
source /root/ws_moveit/install/setup.bash
#source /usr/share/colcon_cd/function/colcon_cd.sh
#export _colcon_cd_root=~/ros2_install/src/some_ros_package
#export ROS_DOMAIN_ID=<your_domain_id>
exec "$@"
