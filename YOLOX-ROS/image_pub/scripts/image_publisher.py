#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Megvii, Inc. and its affiliates.

# ROS2 rclpy -- Ar-Ray-code 2021

import os
import time
from loguru import logger

import cv2
from numpy import empty


import rospy

from std_msgs.msg import Header
from cv_bridge import CvBridge
from sensor_msgs.msg import Image



def talker():
    bridge = CvBridge()
    pub = rospy.Publisher('image_raw', Image, queue_size=10)
    rospy.init_node('image_publisher', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    
    image_path = ''
    image_path_ = rospy.get_param('~image_path_', image_path)
    img_rgb = cv2.imread(image_path_) 
    pub.publish(bridge.cv2_to_imgmsg(img_rgb,"bgr8"))
    """
    while not rospy.is_shutdown():
        hello_str = "hello world %s" % rospy.get_time()
        rospy.loginfo(hello_str)
        pub.publish(hello_str)
        rate.sleep()
    """

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
